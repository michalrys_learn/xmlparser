package com.michalrys;

import org.jdom2.Document;
import org.jdom2.Element;
import org.jdom2.input.DOMBuilder;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.File;
import java.io.IOException;
import java.util.List;

public class ReadXmlFileFromWeb {
    public static void main(String[] args) {
        // https://howtodoinjava.com/xml/jdom2-read-parse-xml-examples/
        // example data: http://www.nbp.pl/kursy/xml/c070z190409.xml

        File file = new File("./src/main/resources/dane.xml");
        System.out.println(file.getAbsoluteFile());

        //Document domParsedDocument = getDOMParsedDocument("./src/main/resources/dane.xml");
        Document domParsedDocument = getDOMParsedDocument("http://www.nbp.pl/kursy/xml/c070z190409.xml");


        List<Element> positionA = domParsedDocument.getRootElement().getChildren().get(3).getChildren();
        String position1CurrencyName = positionA.get(0).getValue();
        String position1CurrencyCode = positionA.get(2).getValue();
        String position1CurrencyBuyValue = positionA.get(3).getValue();
        String position1CurrencySellValue = positionA.get(4).getValue();

        System.out.println(position1CurrencyName);
        System.out.println(position1CurrencyCode);
        System.out.println(position1CurrencyBuyValue);
        System.out.println(position1CurrencySellValue);

    }

    private static Document getDOMParsedDocument(final String fileName)
    {
        Document document = null;
        try
        {
            DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
            //If want to make namespace aware.
            //factory.setNamespaceAware(true);
            DocumentBuilder documentBuilder = factory.newDocumentBuilder();
            org.w3c.dom.Document w3cDocument = documentBuilder.parse(fileName);
            document = new DOMBuilder().build(w3cDocument);
        }
        catch (IOException | SAXException | ParserConfigurationException e)
        {
            e.printStackTrace();
        }
        return document;
    }
}